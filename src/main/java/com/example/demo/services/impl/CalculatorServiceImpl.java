package com.example.demo.services.impl;

import com.example.demo.services.CalculatorService;
import com.example.demo.services.aspects.Println;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService {

    @Println
    @Override
    public int multiplication(int x, int y) {
        return x * y;
    }
}
