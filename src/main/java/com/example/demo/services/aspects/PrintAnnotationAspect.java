package com.example.demo.services.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class PrintAnnotationAspect {
    @Before(value = "@annotation(com.example.demo.services.aspects.Println)")
    public void multiplication(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        System.out.println("გადაცემული არგუმენტები: " + "[" + args[0] + ", " + args[1] + "]");
    }
}
