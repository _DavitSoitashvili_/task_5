package com.example.demo;

import com.example.demo.services.CalculatorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CalculatorServiceTest {

    @Autowired
    CalculatorService calculatorService;


    @Test
    public void testMultiplication() {
        // Arrange
        int expectedResult = 10;
        int x = 2;
        int y = 5;
        // Act
        int result = calculatorService.multiplication(x,y);
        // Assert
        assertEquals(result,expectedResult);
    }
}
